// app.js
let lotion = require('lotion')
let app = lotion({
        initialState: {
		users: [
			{
				username: 'admin',
				plainPassword: 'admin',
				joinedProject: [],
				votedProject: [],
				projectCreateRight: 1000000
			}
		]
		,
              	projects: [
			//{
				// projectID: '',
				// projectName: '',
				// projectActiveStatus: false,
				// projectPhaseNumber: 0,
				// projectOwner: '',
				// projectInfo: '',
				// currentProjectPhase: 0,
				// currentPhaseNeedApprove: false,
				// currentPhaseVoteScore: 0,
				// joinedUser: [] 
			//}
		]
	}
})

function transactionHandler(state, transaction) {
	try {
		if(transaction.currentPhaseNeedApprove !== undefined && transaction.isTransact === undefined) {
			const projects = state.projects
			const index = projects.findIndex(ele => ele.projectID === transaction.projectID)
			if(index !== -1) {
				const project = state.projects[index]
				project.currentPhaseNeedApprove = transaction.currentPhaseNeedApprove
			}
		} else if(transaction.vote !== undefined) {
			const users = state.users
			const uindex = users.findIndex(ele => ele.username === transaction.username)
			if(uindex !== -1) {
				const projects = state.projects
				const pindex = projects.findIndex(ele => ele.projectID === transaction.projectID)
				if(pindex !== -1) {
					if(state.projects[pindex].currentPhaseVoteScore === transaction.acceptedValue) {
						const project = state.projects[pindex]
						project.currentProjectPhase += 1
						project.currentPhaseNeedApprove = false
					}				
					else {
						if(transaction.vote === true) {
							state.projects[pindex].currentPhaseVoteScore += 1
							state.users[uindex].votedProject.push(transaction.projectID)
						}
					}
				}
			}
		} else if(transaction.vote === undefined && transaction.isTransact === undefined ) {
		 	const users = state.users
			const uindex = users.findIndex(ele => ele.username === transaction.username)
			if(uindex !== -1) {
				const projects = state.projects
				const pindex = projects.findIndex(ele => ele.projectID === transaction.projectID)
					if(pindex !== -1) {
						const user = state.users[uindex]
						user.joinedProject.push(transaction.projectID)
						const project = state.projects[pindex]
						project.joinedUser.push(transaction.username)
					}
			}
		} else if(transaction.isTransact === true && transaction.username !== undefined) {
			const users = state.users
			const index = users.findIndex(ele => ele.username === transaction.username)
			if(index !== -1) {
				state.users[index] = transaction
			} else {
				state.users.push(transaction)
			}
		} else if(transaction.isTransact === true && transaction.projectID !== undefined) {
			const projects = state.projects
			const index = projects.findIndex(ele => ele.projectID === transaction.projectID)
			if(index !== -1) {
				state.projects[index] = transaction
			} else {
				state.projects.push(transaction)
			}
		} 
	} catch(err) {
		console.log(err)
	}
}

app.use(transactionHandler)
app.start().then(appInfo => console.log(appInfo.GCI))

