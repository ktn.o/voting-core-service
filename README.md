# voting service
   A voting service on Tendermint blockchain using Lotion.js framework.

## Installaation
   #### Lotion requires node v7.6.0 or higher, and a mac or linux machine.

   npm i lotion-cli -g
   npm i

### Server
   At the root
   node server.js

### Client
   At /client
   node client.js