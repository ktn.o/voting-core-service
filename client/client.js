const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(bodyParser.urlencoded({
        extended: false
}))
app.use(bodyParser.json())
app.use(cors())
app.listen(3000, console.log('Express Server running at http://localhost:3000'))

const { connect } = require('lotion')
const gci = '11e7c740b26ac274ec9fa2c08557f0fec4c2df4df80f096d80bb116f3068e2c2'

async function getUser(req) {
	try {
		const { state } = await connect(gci)
		const users = await state.users
		const index = users.findIndex(ele => ele.username === req.body.username)
		if(index !== undefined) {
			return state.users[index]
		}

	} catch(err) {
		console.log(err)
	}
}

async function getProject(req) {
	try {
		const { state } = await connect(gci)
		const projects = await state.projects
		const index = projects.findIndex(ele => ele.projectID === req.body.projectID)
		if(index !== undefined) {
			return state.projects[index]
		}

	} catch(err) {
		console.log(err)
	}
}

app.post('/get-state', async (req, res) => {
        try {
		const user = await getUser(req)
		if(user !== undefined) {
			const { state } = await connect(gci)
			const result = await state
			res.json(result) 
		} else {
			res.send('user is undefined')
		}
	} catch (err) {
                console.log(err)
        }
})

app.post('/transact', async (req ,res) => {
	try {
		const { send } = await connect(gci)
		const projectActiveStatus = (req.body.projectActiveStatus === 'true')
		const currentPhaseNeedApprove = (req.body.currentPhaseNeedApprove === 'true')
		let request
		if(req.body.username !== undefined) {
			request = {
				isTransact: true,
				username: req.body.username,
				plainPassword: req.body.plainPassword,
				joinedProject: req.body.joinedProject,
				votedProject: []
			}
		} else if(req.body.projectID !== undefined) {
			request = {
				isTransact: true,
				projectID: req.body.projectID,
               			projectName: req.body.projectName,
				projectActiveStatus: projectActiveStatus,
				projectPhaseNumber: parseInt(req.body.projectPhaseNumber),
				projectOwner: req.body.projectOwner,
				projectInfo: req.body.projectInfo,
				currentProjectPhase: parseInt(req.body.currentProjectPhase),
				currentPhaseNeedApprove: currentPhaseNeedApprove,
				currentPhaseVoteScore: parseInt(req.body.currentPhaseVoteScore),
				joinedUser: []
			}
		}
		const result = await send(request)
		console.log(request)
		console.log('transaction sent')
		res.json(result)

	} catch (err) {
		res.json(err)
		console.log(err)
	}
})

app.post('/list-project', async (req, res) => {
	try {
		const user = await getUser(req)
		if(user !== undefined) {
			const { state } = await connect(gci)
			const projects = await state.projects
			res.json(projects)
		} else {
			res.send('user is undefined')
		}

	} catch (err) {
		console.log(err)
	}
})

app.post('/list-joined-project', async (req, res) => {
	try {
		const user = await getUser(req)
		if(user !== undefined) {
			res.json(user.joinedProject)
		} else {
			res.send('user is undefined')
		}

	} catch (err) {
		console.log(err)
	}
})

app.post('/get-project-info', async (req, res) => {
	try {
		const user = await getUser(req)
		if(user !== undefined) {
			const project = await getProject(req)
			if(project !== undefined){
				res.json(project.projectInfo)
			} else {
				res.send('projectID is undefined')
			}
		} else {
			res.send('user is undefined')
		}
		
	} catch (err) {
		console.log(err)
	}
})

app.post('/vote', async (req, res) => {
	const acceptedValue = 3
	try {
		const user = await getUser(req)
		if(user !== undefined) {
			let project = await getProject(req)
			if(project !== undefined) {
				if(!user.votedProject.includes(project.projectID)) {
				if(project.joinedUser.includes(user.username)) {
					let acceptedValue = Math.ceil(project.joinedUser.length*3/4)
				if(project.currentPhaseNeedApprove === true) {
					const vote = (req.body.vote === 'true')
					const { send } = await connect(gci)
					const result = await send({
						username: req.body.username,
						plainPassword: req.body.plainPassword,
						projectID: req.body.projectID,
						acceptedValue: acceptedValue,
						vote: vote
					})
					console.log(result)
					project = await getProject(req)
					if(project.currentPhaseNeedApprove === true) {
						res.send('voted successfully!\n' +
							'currentPhaseVoteScore = ' +
							project.currentPhaseVoteScore +
							'/' +
							acceptedValue)
					} else {
						res.send('cannot vote \n' + 
							'currentPhaseVoteScore has reached the acceptedValue\n' +
							'currentPhaseVoteScore = ' +
							project.currentPhaseVoteScore +
							'/' +
							acceptedValue)
					}
				} else {
					res.send('cannot vote\n' + 
						'currentPhaseVoteScore has reached the acceptedValue' +
						'currentPhaseVoteScore = ' +
						project.currentPhaseVoteScore +
						'/' +
						acceptedValue)
				}
				} else {
					res.send('user haven\'t joined the project')
				}
				} else {
					res.send('user already voted')
				}
			} else {
				res.send('project is undefined')
			}
		} else {
			res.send('user is undefined')
		}
		
	} catch(err) {
		console.log(err)
	}
})

app.post('/join', async (req, res) => {
	try {
		const  user = await getUser(req)
		if(user !== undefined) {
			const project = await getProject(req)
			if(project !== undefined) {
				if(user.joinedProject.length !== 0) {
					if(user.joinedProject.includes(req.body.joinedProject)) {
						res.send('project already joined')
					} else {
						const { send } = await connect(gci)
						const result = await send({
							username: req.body.username,
							plainPassword: req.body.plainPassword,
							projectID: req.body.projectID,
							joinedProject: req.body.projectID
						})
						console.log(result)
						const user = await getUser(req)
			 			res.send('joined successfullly\n'+ 
							'[' + user.joinedProject + ']')
					}
				} else {
					const { send } = await connect(gci)
					const result = await send({
						username: req.body.username,
						plainPassword: req.body.plainPassword,
						projectID: req.body.projectID,
						joinedProject: req.body.joinedProject
					})
					console.log(result)
					const user = await getUser(req)
			 		res.send('joined successfullly\n'+ 
						'[' + user.joinedProject + ']')
				}
			} else {
				res.send('project is undefined')
			}
		} else {
			res.send('user is undefined')
		}
		
	} catch(err) {
		console.log(err)
	}
})

app.post('/update-project-approval-status', async (req, res) => {
	try {
		const user = await getUser(req)
		if(user !== undefined) {
			const project = await getProject(req)
			if(project !== undefined) {
					if(project.projectOwner === user.username) {
						const { send } = await connect(gci)
						const currentPhaseNeedApprove = (req.body.currentPhaseNeedApprove === 'true')
						const result = await send({
							projectID: req.body.projectID,
							currentPhaseNeedApprove: currentPhaseNeedApprove
						})
						console.log(result)
						res.send('currentProjectPhase updated')
					} else {
						res.send('projectOwner not valid')
					}
			} else {
				res.send('project is undefined')
			} 
		} else {
			res.send('user is undefined')
		}
	} catch(err) {
		console.log(err)
	}
})
